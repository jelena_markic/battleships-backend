package hr.markic.battleships.constants;

public class ErrorCodes {

    ErrorCodes(){}

    public static final String ERR_USERNAME_TAKEN = "error.username-already-taken";
    public static final String ERR_BAD_REQUEST = "error.bad-request";
    public static final String ERR_UNKNOWN_USER = "error.unknown-user-id";
    public static final String ERR_UNKNOWN_GAME = "error.unknown-game-id";
    public static final String ERR_INTERNAL_SERVER_ERROR = "error.internal-server-error";
}
