package hr.markic.battleships.constants;

public class BoardConstants {

    BoardConstants(){}

    public static final String Q_SHIP = "#";
    public static final String Q_MISS = "O";
    public static final String Q_HIT = "X";
    public static final String Q_EMPTY = ".";

    public static final int BATTLESHIP_NUM = 1;
    public static final int BATTLESHIP_LENGTH = 4;
    public static final int DESTROYER_NUM = 2;
    public static final int DESTROYER_LENGTH = 3;
    public static final int SUBMARINE_NUM = 3;
    public static final int SUBMARINE_LENGTH = 2;
    public static final int PATROL_CRAFT_NUM = 4;
    public static final int PATROL_CRAFT_LENGTH = 1;

    public static final int SALVO_SHOTS_NUMBER = 5;

    public static final String HIT = "HIT";
    public static final String KILL = "KILL";
    public static final String MISS = "MISS";
}
