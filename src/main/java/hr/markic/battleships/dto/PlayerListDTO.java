package hr.markic.battleships.dto;

import hr.markic.battleships.domain.Player;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@AllArgsConstructor
@Getter
@Setter
public class PlayerListDTO {

    private List<Player> players;
}
