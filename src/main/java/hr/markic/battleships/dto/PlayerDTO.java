package hr.markic.battleships.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PlayerDTO {

    @JsonProperty("player_id")
    private Long playerId;
}
