package hr.markic.battleships.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import hr.markic.battleships.enums.GameStatus;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class PlayerGameDTO {

    @JsonProperty("game_id")
    Long gameId;

    @JsonProperty("opponent_id")
    Long opponentPlayerId;

    GameStatus status;
}
