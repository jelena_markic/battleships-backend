package hr.markic.battleships.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Map;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ShotsResponseDTO {

    private Map<String, String> salvo;

    private ShotsResponseDTO.Game game;

    @Getter
    @Setter
    public static class Game {

        @JsonProperty("player_turn")
        @JsonInclude(JsonInclude.Include.NON_NULL)
        private Long playerTurn;

        @JsonInclude(JsonInclude.Include.NON_NULL)
        private Long won;
    }
}
