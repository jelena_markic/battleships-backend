package hr.markic.battleships.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Map;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class GameStatusDTO {

    private PlayerBoardDTO self;

    private PlayerBoardDTO opponent;

    @JsonProperty("game")
    private GamePlayerStatusDTO gamePlayerStatusDTO;

//    private Map<Long, Boolean> autopilot;

}
