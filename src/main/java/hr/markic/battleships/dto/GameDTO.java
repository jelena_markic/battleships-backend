package hr.markic.battleships.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class GameDTO {

    @JsonProperty("player_id")
    private Long challengingPlayerId;

    @JsonProperty("opponent_id")
    private Long opponentPlayerId;

    @JsonProperty("game_id")
    private Long matchId;

    private String starting;
}
