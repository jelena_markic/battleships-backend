package hr.markic.battleships.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class GamePlayerStatusDTO {

    @JsonProperty("player_turn")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Long playerTurn;

    @JsonProperty("won")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Long won;
}
