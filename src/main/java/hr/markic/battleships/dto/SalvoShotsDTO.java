package hr.markic.battleships.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SalvoShotsDTO {

    private String[] salvo;

}
