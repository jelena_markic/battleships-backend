package hr.markic.battleships.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import hr.markic.battleships.domain.Game;
import hr.markic.battleships.enums.GameStatus;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
public class AllGameListDTO {

    private List<GameDetails> games;

    @Getter
    @Setter
    @AllArgsConstructor
    public static class GameDetails {

        private Long gameId;

        private Long challengingPlayerId;

        private String challengingPlayerName;

        private Long opponentPlayerId;

        private String opponentPlayerName;

        private Boolean challengingPlayerAutopilot;

        private Boolean opponentPlayerAutopilot;

        private GameStatus gameStatus;
    }

}
