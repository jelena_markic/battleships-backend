package hr.markic.battleships.dto;

import hr.markic.battleships.constants.BoardConstants;
import hr.markic.battleships.util.BoardUtil;
import lombok.Getter;
import lombok.Setter;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

@Getter
@Setter
public class BoardDTO {

    private Map<String, Object> board;

    public BoardDTO(){
        board = new HashMap<>();
    }

    public BoardDTO(Map<String, Object> board){
        this.board = board;
    }

    public void addShip(int row, String column, String shipType){
        board.put(getKey(row, column), BoardConstants.Q_SHIP + shipType);
    }

    public void addShip(int row, int column, String shipType){
        char columnStr = (char)(column - 1 + 'A');
        addShip(row, String.valueOf(columnStr), shipType);
    }

    public void addMiss(int row, String column){
        board.put(getKey(row, column), BoardConstants.Q_MISS);
    }

    public void addHit(int row, String column, String shipType){
        board.put(getKey(row, column), BoardConstants.Q_HIT + shipType);
    }

    public void addEmpty(int row, String column){
        board.put(getKey(row, column), BoardConstants.Q_EMPTY);
    }

    private String getKey(int row, String column){
        return row + column;
    }

    public String getValue(int row, String column, boolean opponent, boolean removeShipType){
        String value = (String) board.get(getKey(row, column));
        if (removeShipType && Objects.nonNull(value)){
            value = value.substring(0, 1);
        }

        if (!opponent)
            return value;
        else
            return BoardUtil.obfuscateOpponentCell(value);
    }

    public String getValue(int row, int column, boolean opponent, boolean removeShipType){
        char columnStr = (char)(column - 1 + 'A');
        return getValue(row, String.valueOf(columnStr), opponent, removeShipType);
    }

}
