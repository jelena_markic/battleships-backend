package hr.markic.battleships.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
public class GameListDTO {

    private List<PlayerGameDTO> games;

}
