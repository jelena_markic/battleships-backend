package hr.markic.battleships.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class CoordinateDTO {

    private int row;
    private int column;

}
