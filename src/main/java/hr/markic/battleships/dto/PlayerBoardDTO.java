package hr.markic.battleships.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class PlayerBoardDTO {

    @JsonProperty("player_id")
    private Long playerId;

    private String[] board;

}
