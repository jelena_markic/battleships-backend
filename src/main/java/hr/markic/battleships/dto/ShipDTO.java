package hr.markic.battleships.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@AllArgsConstructor
public class ShipDTO {

    private String name;

    private int num;

    private int length;

    private String code;
}
