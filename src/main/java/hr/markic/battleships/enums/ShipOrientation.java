package hr.markic.battleships.enums;

public enum ShipOrientation {
    VERTICAL,
    HORIZONTAL
}
