package hr.markic.battleships.enums;

public enum GameStatus {
    IN_PROGRESS,
    WON,
    LOST
}
