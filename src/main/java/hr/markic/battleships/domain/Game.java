package hr.markic.battleships.domain;

import hr.markic.battleships.dto.BoardDTO;
import hr.markic.battleships.enums.GameStatus;
import hr.markic.battleships.util.BoardUtil;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.io.Serializable;

@Getter
@Setter
@Entity
@Table(name = "games")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Game implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @ManyToOne
    Player challengingPlayer;

    @ManyToOne
    Player opponentPlayer;

    @ManyToOne
    Player startingPlayer;

    @ManyToOne
    Player wonPlayer;

    @Enumerated(EnumType.STRING)
    GameStatus gameStatus;

    @Column(length = 1500)
    String challengingPlayerBoard;

    @Column(length = 1500)
    String opponentPlayerBoard;

    @Column
    Boolean challengingPlayerAutopilot;

    @Column
    Boolean opponentPlayerAutopilot;

    public BoardDTO getPlayerBoardByPlayer(Long playerId){
        String boardString = playerId.equals(challengingPlayer.getId())? challengingPlayerBoard : opponentPlayerBoard;
        return BoardUtil.boardStringToDTO(boardString);
    }

    public BoardDTO getOpponentPlayerBoardByPlayer(Long playerId){
        String boardString = playerId.equals(challengingPlayer.getId())? opponentPlayerBoard : challengingPlayerBoard;
        return BoardUtil.boardStringToDTO(boardString);
    }

    public void setOpponentPlayerBoardByPlayer(BoardDTO board, Long playerId){
        String boardString = BoardUtil.boardDTOToString(board);
        if (playerId.equals(challengingPlayer.getId())){
            opponentPlayerBoard = boardString;
        } else {
            challengingPlayerBoard = boardString;
        }
    }

    public Long getOpponentPlayerByPlayer(Long playerId){
        return playerId.equals(challengingPlayer.getId())? opponentPlayer.getId() : challengingPlayer.getId();
    }
}
