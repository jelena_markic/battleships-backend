package hr.markic.battleships.service;

import hr.markic.battleships.domain.Game;
import hr.markic.battleships.domain.Player;
import hr.markic.battleships.dto.GameListDTO;
import hr.markic.battleships.dto.GameStatusDTO;

import java.util.List;
import java.util.Optional;

public interface GameService {

    Game startGame(Player challengingPlayer, Player opponentPlayer);

    Optional<Game> findById(Long gameId);

    GameStatusDTO prepareGameStatus(Player player, Game game);

    List<Game> findAllByPlayer(Player player);

    GameListDTO prepareGameListByPlayer(Player player, List<Game> games);

    Game saveGame(Game game);

    void turnAutopilotOn(Player player, Game game);

    List<Game> findAll();
}
