package hr.markic.battleships.service;

import hr.markic.battleships.domain.Game;
import hr.markic.battleships.domain.Player;
import hr.markic.battleships.dto.SalvoShotsDTO;
import hr.markic.battleships.dto.ShotsResponseDTO;

public interface ShotService {

    boolean validateShotSalvos(SalvoShotsDTO salvoShotsDTO);

    ShotsResponseDTO fireShots(Player player, Game game, SalvoShotsDTO salvoShotsDTO);
}
