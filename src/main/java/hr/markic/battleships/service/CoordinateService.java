package hr.markic.battleships.service;

import hr.markic.battleships.dto.BoardDTO;
import hr.markic.battleships.dto.CoordinateDTO;

public interface CoordinateService {

    boolean checkCoordinateInsideBoard(CoordinateDTO coordinateDTO);

    boolean checkCoordinateIsFree(CoordinateDTO coordinateDTO, BoardDTO board);

    boolean checkSiblingCoordinatesFree(CoordinateDTO coordinateDTO, BoardDTO board);
}
