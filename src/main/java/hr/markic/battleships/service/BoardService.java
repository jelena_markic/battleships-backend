package hr.markic.battleships.service;

import hr.markic.battleships.dto.BoardDTO;

public interface BoardService {

    String[] getBoardArray(BoardDTO playerBoard, boolean opponent);

    BoardDTO generateBoard();

}
