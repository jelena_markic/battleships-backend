package hr.markic.battleships.service;

import hr.markic.battleships.domain.Game;
import hr.markic.battleships.domain.Player;

public interface AutoPilotService {

    void runAutoPilot(Player player, Game game);

    void setAutoPilotFlag(Player player, Game game, Boolean flag);

}
