package hr.markic.battleships.service;

import hr.markic.battleships.domain.Player;

import java.util.List;
import java.util.Optional;

public interface PlayerService {

    Optional<Player> findByEmail(String email);

    Optional<Player> findById(Long id);

    List<Player> findAll();

    Player registerPlayer(Player player);
}
