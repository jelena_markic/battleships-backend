package hr.markic.battleships.service.impl;

import hr.markic.battleships.constants.BoardConstants;
import hr.markic.battleships.dto.BoardDTO;
import hr.markic.battleships.dto.CoordinateDTO;
import hr.markic.battleships.service.CoordinateService;
import org.springframework.stereotype.Service;

@Service
public class CoordinateServiceImpl implements CoordinateService {

    @Override
    public boolean checkCoordinateInsideBoard(CoordinateDTO coordinateDTO) {
        int column = coordinateDTO.getColumn();
        int row = coordinateDTO.getRow();

        return column >= 1 && column <= 10 && row >= 1 && row <= 10;
    }

    @Override
    public boolean checkCoordinateIsFree(CoordinateDTO coordinateDTO, BoardDTO board) {
        int column = coordinateDTO.getColumn();
        int row = coordinateDTO.getRow();

        String value = board.getValue(row, column, false, true);
        return value == null || value.equals(BoardConstants.Q_EMPTY);
    }

    @Override
    public boolean checkSiblingCoordinatesFree(CoordinateDTO coord, BoardDTO board) {
        CoordinateDTO coordT = new CoordinateDTO(coord.getRow()-1, coord.getColumn());
        CoordinateDTO coordB = new CoordinateDTO(coord.getRow()+1, coord.getColumn());
        CoordinateDTO coordR = new CoordinateDTO(coord.getRow(), coord.getColumn()+1);
        CoordinateDTO coordL = new CoordinateDTO(coord.getRow(), coord.getColumn()-1);

        return checkCoordinateIsFree(coordT, board) && checkCoordinateIsFree(coordB, board) &&
                checkCoordinateIsFree(coordR, board) && checkCoordinateIsFree(coordL, board);
    }
}
