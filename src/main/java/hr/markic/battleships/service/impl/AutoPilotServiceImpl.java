package hr.markic.battleships.service.impl;

import hr.markic.battleships.config.AppProperties;
import hr.markic.battleships.domain.Game;
import hr.markic.battleships.domain.Player;
import hr.markic.battleships.dto.SalvoShotsDTO;
import hr.markic.battleships.enums.GameStatus;
import hr.markic.battleships.service.AutoPilotService;
import hr.markic.battleships.service.GameService;
import hr.markic.battleships.service.ShotService;
import hr.markic.battleships.util.RandomUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

@Service
public class AutoPilotServiceImpl implements AutoPilotService {

    private static final Logger log = LoggerFactory.getLogger(AutoPilotServiceImpl.class);

    ShotService shotService;

    AppProperties appProperties;

    GameService gameService;

    @Autowired
    public AutoPilotServiceImpl(@Lazy ShotService shotService,
                                AppProperties appProperties,
                                @Lazy GameService gameService){
        this.shotService = shotService;
        this.appProperties = appProperties;
        this.gameService = gameService;
    }

    @Async
    @Override
    public void runAutoPilot(Player player, Game game) {
        log.info("Running autopilot for player " + player.getId() + " for game " + game.getId());

        try {
            while (game.getGameStatus().equals(GameStatus.IN_PROGRESS)){
                if (game.getStartingPlayer().getId().equals(player.getId())){
                    SalvoShotsDTO salvoShotsDTO = new SalvoShotsDTO();
                    salvoShotsDTO.setSalvo(RandomUtil.getRandomSalvoShot());

                    shotService.fireShots(player, game, salvoShotsDTO);
                } else {
                    Thread.sleep(appProperties.getAsync().getThreadSleep());
                    game = gameService.findById(game.getId()).get();
                }
            }
        } catch (InterruptedException e){
            log.debug("Autopilot interrupted.");
        }
        setAutoPilotFlag(player, game, false);
        log.debug("Ending autopilot");
    }

    @Override
    public void setAutoPilotFlag(Player player, Game game, Boolean flag) {
        if (game.getChallengingPlayer().getId().equals(player.getId())){
            game.setChallengingPlayerAutopilot(flag);
        } else {
            game.setOpponentPlayerAutopilot(flag);
        }
        gameService.saveGame(game);
    }
}
