package hr.markic.battleships.service.impl;

import hr.markic.battleships.domain.Game;
import hr.markic.battleships.dto.GameListDTO;
import hr.markic.battleships.dto.PlayerGameDTO;
import hr.markic.battleships.enums.GameStatus;
import hr.markic.battleships.domain.Player;
import hr.markic.battleships.dto.BoardDTO;
import hr.markic.battleships.dto.GamePlayerStatusDTO;
import hr.markic.battleships.dto.GameStatusDTO;
import hr.markic.battleships.dto.PlayerBoardDTO;
import hr.markic.battleships.repository.GameRepository;
import hr.markic.battleships.service.AutoPilotService;
import hr.markic.battleships.service.BoardService;
import hr.markic.battleships.service.GameService;
import hr.markic.battleships.util.BoardUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

@Service
public class GameServiceImpl implements GameService {

    GameRepository gameRepository;

    BoardService boardService;

    AutoPilotService autoPilotService;

    @Autowired
    public GameServiceImpl(GameRepository gameRepository,
                           BoardService boardService,
                           AutoPilotService autoPilotService){
        this.gameRepository = gameRepository;
        this.boardService = boardService;
        this.autoPilotService = autoPilotService;
    }

    @Override
    @Transactional
    public Game startGame(Player challengingPlayer, Player opponentPlayer) {
        Game game = new Game();
        game.setChallengingPlayer(challengingPlayer);
        game.setOpponentPlayer(opponentPlayer);
        game.setGameStatus(GameStatus.IN_PROGRESS);

        BoardDTO challPlBoard = boardService.generateBoard();
        BoardDTO oppPlBoard = boardService.generateBoard();
        game.setChallengingPlayerBoard(BoardUtil.boardDTOToString(challPlBoard));
        game.setOpponentPlayerBoard(BoardUtil.boardDTOToString(oppPlBoard));

        game.setStartingPlayer(BoardUtil.returnRandomStartingBoardPlayer(challengingPlayer, opponentPlayer));

        return gameRepository.save(game);
    }

    @Override
    public Optional<Game> findById(Long gameId) {
        return gameRepository.findById(gameId);
    }

    @Override
    public GameStatusDTO prepareGameStatus(Player player, Game game) {
        GameStatusDTO gameStatusDTO = new GameStatusDTO();
        BoardDTO playerBoard = game.getPlayerBoardByPlayer(player.getId());
        BoardDTO opponentBoard = game.getOpponentPlayerBoardByPlayer(player.getId());
        PlayerBoardDTO playerBoardDTO = new PlayerBoardDTO(
                player.getId(), boardService.getBoardArray(playerBoard, false));
        PlayerBoardDTO opponentBoardDTO = new PlayerBoardDTO(
                game.getOpponentPlayerByPlayer(player.getId()), boardService.getBoardArray(opponentBoard, true));

        gameStatusDTO.setSelf(playerBoardDTO);
        gameStatusDTO.setOpponent(opponentBoardDTO);
        GamePlayerStatusDTO gamePlayerStatusDTO = new GamePlayerStatusDTO();
        if (game.getGameStatus().equals(GameStatus.IN_PROGRESS)){
            gamePlayerStatusDTO.setPlayerTurn(game.getStartingPlayer().getId());
        } else {
            gamePlayerStatusDTO.setWon(game.getWonPlayer().getId());
        }
        gameStatusDTO.setGamePlayerStatusDTO(gamePlayerStatusDTO);

//        Map<Long, Boolean> autoPilot = new HashMap<>();
//        autoPilot.put(game.getChallengingPlayer().getId(), game.getChallengingPlayerAutopilot() != null? game.getChallengingPlayerAutopilot() : false);
//        autoPilot.put(game.getOpponentPlayer().getId(), game.getOpponentPlayerAutopilot() != null? game.getOpponentPlayerAutopilot() : false);
//        gameStatusDTO.setAutopilot(autoPilot);

        return gameStatusDTO;
    }

    @Override
    public List<Game> findAllByPlayer(Player player) {
        return gameRepository.findAllByChallengingPlayerOrOpponentPlayer(player);
    }

    @Override
    public GameListDTO prepareGameListByPlayer(Player player, List<Game> games) {
        List<PlayerGameDTO> playerGameDTOS = new ArrayList<>();
        for (Game game : games) {
            Long opponentPlayerId = game.getOpponentPlayerByPlayer(player.getId());
            playerGameDTOS.add(new PlayerGameDTO(game.getId(), opponentPlayerId, getGameStatusByPlayer(game, player)));
        }

        return new GameListDTO(playerGameDTOS);
    }

    private GameStatus getGameStatusByPlayer(Game game, Player player) {
        if (game.getWonPlayer() != null){
            return game.getWonPlayer().getId().equals(player.getId()) ? GameStatus.WON : GameStatus.LOST;
        } else {
            return GameStatus.IN_PROGRESS;
        }
    }

    @Override
    @Transactional
    public Game saveGame(Game game) {
        return gameRepository.save(game);
    }

    @Override
    public void turnAutopilotOn(Player player, Game game) {
        Boolean autoPilotOn = game.getChallengingPlayer().getId().equals(player.getId()) ?
                game.getChallengingPlayerAutopilot() : game.getOpponentPlayerAutopilot();
        if (Objects.isNull(autoPilotOn) || !autoPilotOn){
            autoPilotService.runAutoPilot(player, game);
        }

        autoPilotService.setAutoPilotFlag(player, game, true);
    }

    @Override
    public List<Game> findAll() {
        return gameRepository.findAll();
    }

}
