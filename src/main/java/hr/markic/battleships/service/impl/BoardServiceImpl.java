package hr.markic.battleships.service.impl;

import hr.markic.battleships.constants.BoardConstants;
import hr.markic.battleships.dto.BoardDTO;
import hr.markic.battleships.dto.CoordinateDTO;
import hr.markic.battleships.dto.ShipDTO;
import hr.markic.battleships.enums.ShipOrientation;
import hr.markic.battleships.service.BoardService;
import hr.markic.battleships.service.CoordinateService;
import hr.markic.battleships.util.RandomUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class BoardServiceImpl implements BoardService {

    private List<ShipDTO> ships;

    private CoordinateService coordinateService;

    @Autowired
    public BoardServiceImpl(CoordinateService coordinateService){
        initializeShips();
        this.coordinateService = coordinateService;
    }

    private void initializeShips() {
        ships = new ArrayList<>();
        ships.add(new ShipDTO("battleship", BoardConstants.BATTLESHIP_NUM, BoardConstants.BATTLESHIP_LENGTH, "B"));
        ships.add(new ShipDTO("destroyer", BoardConstants.DESTROYER_NUM, BoardConstants.DESTROYER_LENGTH, "D"));
        ships.add(new ShipDTO("submarine", BoardConstants.SUBMARINE_NUM, BoardConstants.SUBMARINE_LENGTH, "S"));
        ships.add(new ShipDTO("patrol_craft", BoardConstants.PATROL_CRAFT_NUM, BoardConstants.PATROL_CRAFT_LENGTH, "P"));
    }

    @Override
    public BoardDTO generateBoard() {
        BoardDTO board = generateEmptyBoard();
        return populateShipsRandomly(board);
    }

    /**
     * Creates a board with 'empty' type of cells.
     * @return
     */
    private BoardDTO generateEmptyBoard(){
        BoardDTO boardDTO = new BoardDTO();

        for (int i = 1; i <= 10 ; i++) {
            for (char col = 'A'; col <= 'J'; col++) {
                boardDTO.addEmpty(i, String.valueOf(col));
            }
        }
        return boardDTO;
    }

    /**
     * Places all types of ships on the board.
     * @param board
     * @return
     */
    private BoardDTO populateShipsRandomly(BoardDTO board) {
        for (ShipDTO ship : ships) {
            for (int i = 0; i < ship.getNum() ; i++) {
                placeShipOnBoard(ship, board, i+1);
            }
        }
        return board;
    }

    /**
     * Places ship on board using random iteration until ship is placed correctly.
     * @param ship
     * @param board
     * @param shipNumber
     */
    private void placeShipOnBoard(ShipDTO ship, BoardDTO board, int shipNumber) {
        boolean shipLocated = false;
        List<CoordinateDTO> shipPlacement = null;

        while (!shipLocated){
            List<CoordinateDTO> coordinates = new ArrayList<>();
            CoordinateDTO baseCoord = new CoordinateDTO(RandomUtil.getRandomNumber(10), RandomUtil.getRandomNumber(10));
            if (checkCoordinateValidity(baseCoord, board)){
                coordinates.add(baseCoord);

                ShipOrientation shipOrientation = RandomUtil.getRandomShipOrientation();
                for (int i = 0; i < ship.getLength() -1; i++) {
                    CoordinateDTO nextCoord = getNextCoordinate(baseCoord, shipOrientation);
                    baseCoord = nextCoord;
                    if (!checkCoordinateValidity(nextCoord, board)){
                        break;
                    } else {
                        coordinates.add(nextCoord);
                    }
                }

                if (coordinates.size() == ship.getLength()){
                    shipLocated = true;
                    shipPlacement = coordinates;
                }
            }
        }

        addShipCoordinatesOnBoard(shipPlacement, board, ship, shipNumber);
    }

    /**
     * Add ship coordinates on board.
     * @param shipPlacement
     * @param board
     * @param ship
     * @param shipNumber
     */
    private void addShipCoordinatesOnBoard(List<CoordinateDTO> shipPlacement, BoardDTO board, ShipDTO ship, int shipNumber) {
        for (CoordinateDTO coordinateDTO : shipPlacement) {
            board.addShip(coordinateDTO.getRow(), coordinateDTO.getColumn(), ship.getCode() + shipNumber);
        }
    }

    /**
     * Creates next coordinate based on the current coordinate and the ship orientation level
     * @param baseCoord
     * @param shipOrientation
     * @return
     */
    private CoordinateDTO getNextCoordinate(CoordinateDTO baseCoord, ShipOrientation shipOrientation) {
        if (shipOrientation.equals(ShipOrientation.HORIZONTAL)){
            return new CoordinateDTO(baseCoord.getRow(), baseCoord.getColumn() + 1);
        } else {
            return new CoordinateDTO(baseCoord.getRow() + 1, baseCoord.getColumn());
        }
    }

    /**
     * Checks if it's feasable to add ship on this coordinate:
     *  - if it's inside the board
     *  - if no existing ship is located next to it
     *  - if coordinate is not occupied
     *
     * @param baseCoord
     * @param board
     * @return
     */
    private boolean checkCoordinateValidity(CoordinateDTO baseCoord, BoardDTO board) {
        return coordinateService.checkCoordinateInsideBoard(baseCoord) &&
                coordinateService.checkCoordinateIsFree(baseCoord, board) &&
                    coordinateService.checkSiblingCoordinatesFree(baseCoord, board);
    }

    @Override
    public String[] getBoardArray(BoardDTO playerBoard, boolean opponent) {
        String[] boardArray = new String[10];

        for (int i = 1; i <= 10 ; i++) {
            StringBuilder sb = new StringBuilder();
            for (char col = 'A'; col <= 'J'; col++) {
                sb.append(playerBoard.getValue(i, String.valueOf(col), opponent, true));
            }
            boardArray[i-1] = sb.toString();
        }
        return boardArray;
    }

}