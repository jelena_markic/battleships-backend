package hr.markic.battleships.service.impl;

import hr.markic.battleships.config.PusherConfig;
import hr.markic.battleships.constants.BoardConstants;
import hr.markic.battleships.domain.Game;
import hr.markic.battleships.domain.Player;
import hr.markic.battleships.dto.BoardDTO;
import hr.markic.battleships.dto.SalvoShotsDTO;
import hr.markic.battleships.dto.ShotsResponseDTO;
import hr.markic.battleships.enums.GameStatus;
import hr.markic.battleships.service.GameService;
import hr.markic.battleships.service.PlayerService;
import hr.markic.battleships.service.ShotService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service
public class ShotServiceImpl implements ShotService {

    private GameService gameService;

    private PlayerService playerService;

    private PusherConfig pusherConfig;

    public ShotServiceImpl(GameService gameService,
                           PlayerService playerService,
                           PusherConfig pusherConfig){
        this.gameService = gameService;
        this.playerService = playerService;
        this.pusherConfig = pusherConfig;
    }

    @Override
    public boolean validateShotSalvos(SalvoShotsDTO salvoShotsDTO) {
        String[] salvo = salvoShotsDTO.getSalvo();
        if (Objects.isNull(salvo)){
            return false;
        }
        for (String shot : salvo) {
            if (!validateShot(shot)){
                return false;
            }
        }
        return salvo.length == BoardConstants.SALVO_SHOTS_NUMBER;
    }

    private boolean validateShot(String shot) {
        String regex = "([1-9]|10)x([A-J])";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(shot);
        return matcher.matches();
    }

    @Override
    @Transactional
    public ShotsResponseDTO fireShots(Player player, Game game, SalvoShotsDTO salvoShotsDTO) {
        ShotsResponseDTO shotsResponseDTO = new ShotsResponseDTO();
        Map<String, String> salvoResponse = new LinkedHashMap<>();

        BoardDTO board = game.getOpponentPlayerBoardByPlayer(player.getId());
        String[] salvo = salvoShotsDTO.getSalvo();
        for (String s : salvo) {
            String[] array = s.split("x");
            int row = Integer.parseInt(array[0]);
            String column = array[1];
            String value = board.getValue(row, column, false, false);

            String shipStatus = value.substring(0, 1);
            String shipType = value.substring(1);
            if (shipStatus.equals(BoardConstants.Q_EMPTY) || shipStatus.equals(BoardConstants.Q_MISS)){
                salvoResponse.put(s, BoardConstants.MISS);
                board.addMiss(row, column);
            } else if (shipStatus.equals(BoardConstants.Q_SHIP)){
                board.addHit(row, column, shipType);
                salvoResponse.put(s, returnHitTypeBasedOnShipStatus(shipType, board));
            } else if (shipStatus.equals(BoardConstants.Q_HIT)){
                salvoResponse.put(s, BoardConstants.MISS);
            }
        }
        shotsResponseDTO.setSalvo(salvoResponse);
        shotsResponseDTO.setGame(setResponseGameEntity(player, board, game));

        game.setOpponentPlayerBoardByPlayer(board, player.getId());
        if (shotsResponseDTO.getGame().getPlayerTurn() != null){
            game.setStartingPlayer(playerService.findById(shotsResponseDTO.getGame().getPlayerTurn()).get());
        } else {
            game.setWonPlayer(playerService.findById(shotsResponseDTO.getGame().getWon()).get());
            if (game.getWonPlayer().getId().equals(game.getChallengingPlayer().getId())){
                game.setGameStatus(GameStatus.WON);
            } else {
                game.setGameStatus(GameStatus.LOST);
            }
        }
        gameService.saveGame(game);

        sendPusherNotifications(game);

        return shotsResponseDTO;
    }

    private void sendPusherNotifications(Game game) {
        pusherConfig.trigger(pusherConfig.getChannel(), pusherConfig.getShotsEvent()+ game.getId(), "SHOTS_FIRED");
    }

    /**
     * Creates {@link hr.markic.battleships.dto.ShotsResponseDTO.Game} entity based ob {@link BoardDTO} status.
     * @param player
     * @param board
     * @param game
     * @return
     */
    private ShotsResponseDTO.Game setResponseGameEntity(Player player, BoardDTO board, Game game) {
        ShotsResponseDTO.Game gameR = new ShotsResponseDTO.Game();
        if (checkIfShipsAreAllHit(board)){
            gameR.setWon(player.getId());
        } else {
            gameR.setPlayerTurn(game.getOpponentPlayerByPlayer(player.getId()));
        }
        return gameR;
    }

    /**
     * Checks if there are any ships on board.
     * @param board
     * @return
     */
    private boolean checkIfShipsAreAllHit(BoardDTO board) {
        Map<String, Object> boardMap = board.getBoard();
        for (Object value : boardMap.values()){
            if (((String)value).contains(BoardConstants.Q_SHIP)){
                return false;
            }
        }
        return true;
    }

    /**
     * Checks if there's any coordinates left untouched for the ship type and returns appropriate shot result.
     * @param shipType
     * @param board
     * @return
     */
    private String returnHitTypeBasedOnShipStatus(String shipType, BoardDTO board) {
        Map<String, Object> boardMap = board.getBoard();
        for (Object s : boardMap.values()) {
            if (s.equals(BoardConstants.Q_SHIP + shipType)){
                return BoardConstants.HIT;
            }
        }
        return BoardConstants.KILL;
    }

}
