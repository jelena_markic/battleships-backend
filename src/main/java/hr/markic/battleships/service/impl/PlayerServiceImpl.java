package hr.markic.battleships.service.impl;

import hr.markic.battleships.domain.Player;
import hr.markic.battleships.repository.PlayerRepository;
import hr.markic.battleships.service.PlayerService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class PlayerServiceImpl implements PlayerService {

    private PlayerRepository playerRepository;

    public PlayerServiceImpl(PlayerRepository playerRepository){
        this.playerRepository = playerRepository;
    }

    @Override
    public Optional<Player> findByEmail(String email) {
        return playerRepository.findPlayerByEmail(email);
    }

    @Override
    public Optional<Player> findById(Long id) {
        return playerRepository.findById(id);
    }

    @Override
    public List<Player> findAll() {
        return playerRepository.findAll();
    }

    @Override
    @Transactional
    public Player registerPlayer(Player player) {
        return playerRepository.save(player);
    }
}
