package hr.markic.battleships.rest.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class UnknownGameException extends Exception {

    private final Long id;

}