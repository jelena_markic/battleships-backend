package hr.markic.battleships.rest.controller;

import hr.markic.battleships.domain.Game;
import hr.markic.battleships.domain.Player;
import hr.markic.battleships.dto.AllGameListDTO;
import hr.markic.battleships.dto.GameDTO;
import hr.markic.battleships.dto.GameListDTO;
import hr.markic.battleships.dto.GameStatusDTO;
import hr.markic.battleships.dto.PlayerDTO;
import hr.markic.battleships.dto.SalvoShotsDTO;
import hr.markic.battleships.dto.ShotsResponseDTO;
import hr.markic.battleships.enums.GameStatus;
import hr.markic.battleships.mapper.GameMapper;
import hr.markic.battleships.rest.exception.InvalidRequestDataException;
import hr.markic.battleships.rest.exception.UnknownGameException;
import hr.markic.battleships.rest.exception.UnknownUserException;
import hr.markic.battleships.service.GameService;
import hr.markic.battleships.service.PlayerService;
import hr.markic.battleships.service.ShotService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@RestController
@RequestMapping(value = "/player")
public class GameController {

    private PlayerService playerService;

    private GameService gameService;

    private GameMapper gameMapper;

    private ShotService shotService;

    @Autowired
    GameController(PlayerService playerService,
                   GameService gameService,
                   GameMapper gameMapper,
                   ShotService shotService){
        this.playerService = playerService;
        this.gameService = gameService;
        this.gameMapper = gameMapper;
        this.shotService = shotService;
    }

    @PostMapping("/{opponentPlayerId}/game")
    public ResponseEntity<GameDTO> startGame(@RequestBody PlayerDTO playerDTO, @PathVariable Long opponentPlayerId)
            throws InvalidRequestDataException, URISyntaxException, UnknownUserException {
        if (Objects.isNull(playerDTO.getPlayerId())){
            throw new InvalidRequestDataException();
        }

        Optional<Player> challengingPlayer = playerService.findById(playerDTO.getPlayerId());
        Optional<Player> opponentPlayer = playerService.findById(opponentPlayerId);

        if (!challengingPlayer.isPresent()){
            throw new UnknownUserException(playerDTO.getPlayerId());
        }
        if (!opponentPlayer.isPresent()){
            throw new UnknownUserException(opponentPlayerId);
        }

        Game game = gameService.startGame(challengingPlayer.get(), opponentPlayer.get());
        GameDTO gameDTO = gameMapper.toDTO(game);
        return ResponseEntity.created(new URI("/game/" + game.getId())).body(gameDTO);
    }

    @GetMapping("/{playerId}/game/{gameId}")
    public ResponseEntity<GameStatusDTO> getGameStatus(@PathVariable Long playerId, @PathVariable Long gameId)
            throws UnknownUserException, UnknownGameException {
        Optional<Player> player = playerService.findById(playerId);
        Optional<Game> game = gameService.findById(gameId);

        if (!player.isPresent()){
            throw new UnknownUserException(playerId);
        }
        if (!game.isPresent()){
            throw new UnknownGameException(gameId);
        }
        if (!game.get().getChallengingPlayer().getId().equals(player.get().getId()) &&
                !game.get().getOpponentPlayer().getId().equals(player.get().getId())){
            return ResponseEntity.badRequest().build();
        }

        GameStatusDTO gameStatus = gameService.prepareGameStatus(player.get(), game.get());
        return ResponseEntity.ok().body(gameStatus);
    }

    @GetMapping("/{playerId}/game/list")
    public ResponseEntity<GameListDTO> gamesList(@PathVariable Long playerId)
            throws UnknownUserException {
        Optional<Player> player = playerService.findById(playerId);

        if (!player.isPresent()){
            throw new UnknownUserException(playerId);
        }

        List<Game> games = gameService.findAllByPlayer(player.get());
        if (Objects.isNull(games) || games.isEmpty()){
            return ResponseEntity.noContent().build();
        }

        GameListDTO gameList = gameService.prepareGameListByPlayer(player.get(), games);
        return ResponseEntity.ok().body(gameList);
    }

    @PutMapping("/{playerId}/game/{gameId}")
    public ResponseEntity<ShotsResponseDTO> fireSalvo(@PathVariable Long playerId, @PathVariable Long gameId,
                                       @RequestBody SalvoShotsDTO salvoShotsDTO)
            throws UnknownUserException, UnknownGameException {
        Optional<Player> player = playerService.findById(playerId);
        Optional<Game> game = gameService.findById(gameId);

        if (!player.isPresent()){
            throw new UnknownUserException(playerId);
        }
        if (!game.isPresent()){
            throw new UnknownGameException(gameId);
        }
        if (!game.get().getChallengingPlayer().getId().equals(player.get().getId()) &&
                !game.get().getOpponentPlayer().getId().equals(player.get().getId())){
            return ResponseEntity.badRequest().build();
        }
        if (!game.get().getStartingPlayer().getId().equals(playerId)){
            return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
        }
        if (!game.get().getGameStatus().equals(GameStatus.IN_PROGRESS)){
            return ResponseEntity.status(HttpStatus.METHOD_NOT_ALLOWED).build();
        }
        if(!shotService.validateShotSalvos(salvoShotsDTO)){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }

        ShotsResponseDTO shotsResponse = shotService.fireShots(player.get(), game.get(), salvoShotsDTO);
        return ResponseEntity.ok().body(shotsResponse);
    }

    @PutMapping("/{playerId}/game/{gameId}/autopilot")
    public ResponseEntity turnAutopilotOn(@PathVariable Long playerId, @PathVariable Long gameId)
            throws UnknownUserException, UnknownGameException {
        Optional<Player> player = playerService.findById(playerId);
        Optional<Game> game = gameService.findById(gameId);

        if (!player.isPresent()){
            throw new UnknownUserException(playerId);
        }
        if (!game.isPresent()){
            throw new UnknownGameException(gameId);
        }
        if (!game.get().getGameStatus().equals(GameStatus.IN_PROGRESS)){
            return ResponseEntity.status(HttpStatus.METHOD_NOT_ALLOWED).build();
        }

        gameService.turnAutopilotOn(player.get(), game.get());
        return ResponseEntity.noContent().build();
    }

    @GetMapping("/game/list")
    public ResponseEntity<AllGameListDTO> allGames() {
        List<Game> games = gameService.findAll();
        List<AllGameListDTO.GameDetails> gDetails = new ArrayList<>();
        for (Game game : games) {
            gDetails.add(new AllGameListDTO.GameDetails(
                    game.getId(), game.getChallengingPlayer().getId(), game.getChallengingPlayer().getName(),
                    game.getOpponentPlayer().getId(), game.getOpponentPlayer().getName(),
                    game.getChallengingPlayerAutopilot(), game.getOpponentPlayerAutopilot(),
                    game.getGameStatus()));
        }
        AllGameListDTO gameList = new AllGameListDTO(gDetails);
        return ResponseEntity.ok().body(gameList);
    }

}
