package hr.markic.battleships.rest.controller;

import hr.markic.battleships.domain.Player;
import hr.markic.battleships.dto.PlayerListDTO;
import hr.markic.battleships.rest.exception.InvalidRequestDataException;
import hr.markic.battleships.rest.exception.UsernameTakenDataException;
import hr.markic.battleships.service.PlayerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@RestController
@RequestMapping(value = "/player")
public class PlayerController {

    private PlayerService playerService;

    @Autowired
    PlayerController(PlayerService playerService){
        this.playerService = playerService;
    }

    @PostMapping()
    public ResponseEntity registerPlayer(@RequestBody Player player)
            throws InvalidRequestDataException, UsernameTakenDataException, URISyntaxException {
        if (Objects.isNull(player.getEmail()) || Objects.isNull(player.getName())){
            throw new InvalidRequestDataException();
        }

        Optional<Player> existing = playerService.findByEmail(player.getEmail());
        if (existing.isPresent()){
            throw new UsernameTakenDataException(player.getEmail());
        }

        player = playerService.registerPlayer(player);

        return ResponseEntity.created(new URI("/player/" + player.getId())).build();
    }

    @GetMapping("/{playerId}")
    public ResponseEntity<Player> getPlayer(@PathVariable Long playerId){
        Optional<Player> player = playerService.findById(playerId);
        if (!player.isPresent()){
            return ResponseEntity.notFound().build();
        }

        return ResponseEntity.ok().body(player.get());
    }

    @GetMapping("/list")
    public ResponseEntity<PlayerListDTO> getPlayerList(){
        List<Player> players = playerService.findAll();
        PlayerListDTO playerListDTO = new PlayerListDTO(players);

        return ResponseEntity.ok().body(playerListDTO);
    }


}
