package hr.markic.battleships.rest.error;

import hr.markic.battleships.constants.ErrorCodes;
import hr.markic.battleships.rest.exception.InvalidRequestDataException;
import hr.markic.battleships.rest.exception.UnknownGameException;
import hr.markic.battleships.rest.exception.UnknownUserException;
import hr.markic.battleships.rest.exception.UsernameTakenDataException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class CommonErrorHandler {

    private final Logger log = LoggerFactory.getLogger(CommonErrorHandler.class);

    @ExceptionHandler(UsernameTakenDataException.class)
    public ResponseEntity<ApiErrorResponse> handleUsernameTakenException(UsernameTakenDataException ex) {
        ApiErrorResponse response = new ApiErrorResponse(ErrorCodes.ERR_USERNAME_TAKEN, ex.getEmail());
        return new ResponseEntity<>(response, HttpStatus.CONFLICT);
    }

    @ExceptionHandler({InvalidRequestDataException.class, HttpMessageNotReadableException.class,
                         HttpMediaTypeNotSupportedException.class})
    public ResponseEntity<ApiErrorResponse> handleInvalidRequestApiException(Exception ex) {
        ApiErrorResponse response = new ApiErrorResponse(ErrorCodes.ERR_BAD_REQUEST,
                        "Request body not readable / not fully populated.");
        return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler({UnknownUserException.class})
    public ResponseEntity<ApiErrorResponse> handleUnknownUserException(UnknownUserException ex) {
        ApiErrorResponse response = new ApiErrorResponse(ErrorCodes.ERR_UNKNOWN_USER,
                ex.getId().toString());
        return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler({UnknownGameException.class})
    public ResponseEntity<ApiErrorResponse> handleUnknownGameException(UnknownGameException ex) {
        ApiErrorResponse response = new ApiErrorResponse(ErrorCodes.ERR_UNKNOWN_GAME,
                ex.getId().toString());
        return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
    }
    @ExceptionHandler(Exception.class)
    public ResponseEntity<ApiErrorResponse> handleApiException(Exception ex) {
        ApiErrorResponse response = new ApiErrorResponse(ErrorCodes.ERR_INTERNAL_SERVER_ERROR,
                ex.toString());
        log.error("Failed to handle request.", ex);
        return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
    }
}