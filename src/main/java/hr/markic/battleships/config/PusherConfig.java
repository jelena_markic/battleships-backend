package hr.markic.battleships.config;

import com.pusher.rest.Pusher;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class PusherConfig {

    private final Logger log = LoggerFactory.getLogger(PusherConfig.class);
    private Pusher pusher;

    private AppProperties appProperties;

    @Autowired
    private PusherConfig(AppProperties appProperties){
        this.appProperties = appProperties;
        init();
    }

    private void init() {
        pusher = new Pusher(appProperties.getPusher().getAppId(),
                appProperties.getPusher().getKey(),
                appProperties.getPusher().getSecret());
        pusher.setCluster(appProperties.getPusher().getCluster());
        pusher.setEncrypted(true);
    }

    public void trigger(String channel, String event, Object object){
        pusher.trigger(channel, event, object);
    }

    public String getChannel(){
        return appProperties.getPusher().getChannel();
    }

    public String getShotsEvent(){
        return appProperties.getPusher().getShotsEvent();
    }

}
