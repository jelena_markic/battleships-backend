package hr.markic.battleships.repository;

import hr.markic.battleships.domain.Game;
import hr.markic.battleships.domain.Player;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface GameRepository extends JpaRepository<Game, Long> {

    @Query("from Game g where g.challengingPlayer = :player or g.opponentPlayer = :player")
    List<Game> findAllByChallengingPlayerOrOpponentPlayer(@Param("player") Player player);
}