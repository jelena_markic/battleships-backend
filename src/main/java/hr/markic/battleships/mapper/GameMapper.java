package hr.markic.battleships.mapper;

import hr.markic.battleships.domain.Game;
import hr.markic.battleships.dto.GameDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

@Mapper(componentModel = "spring")
public interface GameMapper {

    @Mappings({
            @Mapping(target="matchId", source="id"),
            @Mapping(target="starting", source="startingPlayer.id"),
            @Mapping(target="challengingPlayerId", source="challengingPlayer.id"),
            @Mapping(target="opponentPlayerId", source="opponentPlayer.id")
    })
    GameDTO toDTO(Game game);
}
