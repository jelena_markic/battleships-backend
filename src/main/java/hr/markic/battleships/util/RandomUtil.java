package hr.markic.battleships.util;

import hr.markic.battleships.enums.ShipOrientation;

import java.util.Random;

public class RandomUtil {

    RandomUtil(){}

    private static Random rand = new Random();

    public static int getRandomNumber(int top){
        return rand.nextInt(top);
    }

    public static ShipOrientation getRandomShipOrientation(){
        int num = getRandomNumber(2);
        return num == 1 ? ShipOrientation.HORIZONTAL : ShipOrientation.VERTICAL;
    }

    public static String[] getRandomSalvoShot() {
        String[] shots = new String[5];
        for (int i = 0; i < 5 ; i++) {
            String row = String.valueOf(getRandomNumber(10) + 1);
            String cell = String.valueOf((char)('A' + getRandomNumber(10)));
            shots[i] = row + "x" + cell;
        }
        return shots;
    }
}
