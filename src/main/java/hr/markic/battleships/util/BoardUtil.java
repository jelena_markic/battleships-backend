package hr.markic.battleships.util;

import hr.markic.battleships.domain.Player;
import hr.markic.battleships.constants.BoardConstants;
import hr.markic.battleships.dto.BoardDTO;

import java.util.Map;

public class BoardUtil {

    BoardUtil(){}

    public static BoardDTO boardStringToDTO(String boardString){
        Map<String, Object> map = JsonUtil.jsonStringToMap(boardString);
        return new BoardDTO(map);
    }

    public static String boardDTOToString(BoardDTO boardDTO){
        return JsonUtil.mapToJsonString(boardDTO.getBoard());
    }

    public static String obfuscateOpponentCell(String cellValue){
        if (cellValue.equals(BoardConstants.Q_SHIP))
            return BoardConstants.Q_EMPTY;
        else
            return cellValue;
    }

    public static Player returnRandomStartingBoardPlayer(Player player1, Player player2){
        int num = RandomUtil.getRandomNumber(2);
        return num == 1 ? player1 : player2;
    }

}
