package hr.markic.battleships.util;

import org.json.JSONObject;

import java.util.Map;

public class JsonUtil {

    JsonUtil(){}

    public static String mapToJsonString(Map<String, Object> map){
        JSONObject object = new JSONObject(map);
        return object.toString();
    }

    public static Map<String, Object> jsonStringToMap(String json){
        JSONObject object = new JSONObject(json);
        return object.toMap();
    }
}
